from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.stacklayout import StackLayout
from kivy.properties import ObjectProperty

import yahtzeelogic as Y

class YahtzeeUI(BoxLayout):
    
    def __init__(self, game, *args, **kwargs):
        self.game = game
        self.orientation = 'horizontal'
        super().__init__(*args, **kwargs)
        
        score = YahtzeeScore(game, size_hint=(0.7, 1))
        self.add_widget(score)
        dice = YahtzeeDice(game, size_hint=(0.3, 1))
        self.add_widget(dice)

class YahtzeeScore(GridLayout):
    
    rollnames = ['Aces', 'Twos', 'Threes', 'Fours', 'Fives', 'Sixes',
                 'Bonus', 'Pair', 'Double Pair', 'Three-Of-A-Kind',
                 'Four-Of-A-Kind', 'Full House', 'Small Straight',
                 'Large Straight', 'Chance', 'Yatzee', 'Total']
    
    def __init__(self, game, *args, **kwargs):
        
        super().__init__(*args, **kwargs)
        self.game = game
        self.cols = self.game.n_players + 1
        
        self.add_widget(Label(text='', size_hint_x=1.5))
        
        for i in range(self.cols-1):
            self.add_widget(Label(text=self.game.players[i], multiline=False))
        
        for i in range(17):
            self.add_widget(Label(text=self.rollnames[i], size_hint_x=1.5))
            for j in range(self.cols-1):
                _id = 'score_' + str(j) + '_' + str(i)
                if i != 6 and i != 16:
                    button = Button(text='0', id=_id)
                    button.bind(on_press=self.game.button_pressed)
                    self.add_widget(button)
                    Y.ids[_id] = button
                else:
                    label = Label(text='0', id=_id)
                    self.add_widget(label)
                    Y.ids[_id] = label

class YahtzeeDice(StackLayout):
    
    def __init__(self, game, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.orientation = 'lr-tb'
        self.game = game
        
        button = Button(text='roll', id='rollbutton', size_hint = (1, .2))
        button.bind(on_press = self.game.button_pressed)
        self.add_widget(button)
        Y.ids['rollbutton'] = button
        
        dicebuttons = BoxLayout(orientation='horizontal', size_hint = (1, .2))
        for i in range(5):
            _id = 'dicebutton_'+str(i)
            button = ToggleButton(text='6', id=_id)
            button.bind(on_press = self.game.button_pressed)
            dicebuttons.add_widget(button)
            Y.ids[_id] = button
        self.add_widget(dicebuttons)

class YahtzeeApp(App):
    
    def build(self):
        game = YahtzeeUI(Y.setup_game())
        return game

YahtzeeApp().run()
