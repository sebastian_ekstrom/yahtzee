class YahtzeeGame:
    def __init__(self, players):
        self.n_players = len(players)
        self.players = players
        self.scores = [[0 for i in range(self.n_players)] for j in range(17)]
        
        self.dice = [6]*5
        self.dice_locked = [False]*5
        
        self.current_player = 0
        self.n_rolls = 0
    
    def button_pressed(self, obj):
        if obj.id == 'rollbutton':
            self.roll_button_pressed()
        elif obj.id.startswith('dicebutton_'):
            nbr = int(obj.id[-1])
            self.dice_button_pressed(nbr)
        else:
            pos = obj.id.rfind('_')
            x = int(obj.id[6:pos])
            y = int(obj.id[pos+1:])
            self.score_button_pressed(x, y)
        #print(obj.id)
        #print(ids[obj.id])

    def roll_button_pressed(self):
        print('The roll button was pressed')

    def dice_button_pressed(self, nbr):
        print('Dice button', nbr, 'was pressed')

    def score_button_pressed(self, x, y):
        print('Score button (' + str(x) + ', ' + str(y) + ') was pressed')



def setup_game():
    players = []
    n = int(input('Enter number of players: '))
    for i in range(n):
        players.append(input('Enter name of player ' + str(i+1) + ': '))
    game = YahtzeeGame(players)
    return game

def update_dice(nbr, text):
    ids['dicebutton_' + str(nbr)].text = text

def update_score(x, y, text):
    ids['score_' + str(x) + '_' + str(y)].text = text

def update(_id, text):
    ids[_id] = text

def set_dice_button_lock(nbr, locked):
    _id = 'dicebutton_' + str(nbr)
    if locked:
        ids[_id].state = 'down'
    else:
        ids[_id].state = 'normal'

def get_dice_button_lock(nbr):
    return ids['dicebutton_' + str(nbr)].state == 'down'

def set_roll_button_disable(disabled):
    ids['rollbutton'].disabled = disabled

def set_score_button_disable(x, y, disabled):
    _id = 'score_' + str(x) + '_' + str(y)
    ids[_id].disabled = disabled

ids = {}
